package geometry;

public class Triangle{

    private double height;
    private double base;

    public Triangle(double height, double base){
        this.height = height;
        this.base = base;
    }
    public double getHeight(){
        return this.height;
    }
    public double getBase(){
        return this.base;
    }

    public double getArea(){
        return (this.height+this.base)/2;
    }
    public String toString(){
        return "the height of the triangle is: " + this.height + " and the base is: " + this.base;
    }
}