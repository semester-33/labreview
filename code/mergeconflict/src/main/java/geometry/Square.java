package geometry;


public class Square {
    
    public double side;


    public Square(double side){
        this.side = side;
    }
    
    public double getSide() {
        return this.side;
    }

    public double getArea() {
        return (this.side * 4);
    }

    public String toString() {
        String s = "Side of square: " + this.side;
        return s;
    }

}
