package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class TriangleTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void triangleConstructGood(){
        Triangle t = new Triangle(2, 3);
        assertEquals(2, t.getHeight(), 0.000001);
        assertEquals(3, t.getBase(), 0.000001);
    }
    @Test
    public void areaIsGood(){
        Triangle t = new Triangle(2, 3);

        assertEquals(2.5, t.getArea(), 0.0000001);
    }
    @Test
    public void triangleToStringGood(){
        Triangle t = new Triangle(2, 3);

        assertEquals("the height of the triangle is: 2.0 and the base is: 3.0", t.toString());

    }

}
