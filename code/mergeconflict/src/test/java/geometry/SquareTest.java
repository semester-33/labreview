package geometry;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class SquareTest
{
    /**
     * Testing because michelle broke the code
     */
    @Test
    public void squareGetMthodeTRUE() {
        Square s = new Square(69.9);
        assertEquals(69.9 ,s.getSide(), 0.01);
    }

    @Test
    public void squareGetAreaTRUE() {
        Square s = new Square(4);
        assertEquals(16 ,s.getArea(), 0.01);
    }

    @Test
    public void squareToStringTRUE() {
        Square s = new Square(4);
        assertEquals("Side of square: 4.0" ,s.toString());
    }
}
